const AWS = require('aws-sdk');
const util = require('util');
const s3 = new AWS.S3();

exports.handler = async (event, context, callback) => {
  console.log("Reading options from event:\n", util.inspect(event, {depth: 5}));
  const srcBucket = event.Records[0].s3.bucket.name;
  const srcKey    = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, " "));
  const dstBucket = srcBucket;
  const dstKey    = "processed-"+srcKey;
  try {
      const params = {
          Bucket: srcBucket,
          Key: srcKey
      };
      var original_telemetry = await s3.getObject(params).promise();
      console.log("getObject executed ");
      const destparams = {
          Bucket: dstBucket,
          Key: dstKey,
          Body: original_telemetry.Body
      };

      const putResult = await s3.putObject(destparams).promise(); 
      
  } catch (error) {
      console.log(error);
      return;
  } 
      
  console.log('Successfully processed ' + srcBucket + '/' + srcKey +
      ' and uploaded to ' + dstBucket + '/' + dstKey);
};