aws lambda create-function --function-name process_telemetry --zip-file fileb://function.zip --handler index.handler --runtime nodejs12.x --timeout 10 --memory-size 1024 --role arn:aws:iam::244873767943:role/lambda-s3-role --cli-binary-format raw-in-base64-out

aws lambda update-function-code \
    --function-name  process_telemetry \
    --zip-file fileb://function.zip